## Breakpoints UI
This module will provide a UI to display all breakpoints provide by modules and themes.

For site builder, this is perfect to find out about your breakpoints in your website.

For developers, this module contains a very light-weight method that allows you to
get all the information about breakpoints in an array. This can be very helpful if you
need to use the breakpoints data for other modules.

## Requirements

Breakpoints Module - Core

## Install

Installing via composer is recommended.

```bash
composer require drupal/breakpoints_ui
```

Install as you would normally install a contributed Drupal module. See:
https://www.drupal.org/documentation/install/modules-themes/modules-8
for further information.

### Drush commands

Drush command is provided to help with debugging or listing out breakpoints.

```bash
drush breakpoints:debug
```

### Maintainers

* George Anderson (geoanders) - https://www.drupal.org/u/geoanders
* Darryl Norris (darol100) - https://www.drupal.org/u/darol100
