<?php

namespace Drupal\breakpoints_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Breakpoints Ui Controller.
 *
 * @package Drupal\breakpoints_ui\Controller
 */
class BreakpointsUiController extends ControllerBase implements TrustedCallbackInterface {

  /**
   * Breakpoints overview.
   *
   * @return array
   *   Information Breakpoints info.
   */
  public function breakpoints(): array {
    $class = get_class($this);
    return [
      '#theme' => 'breakpoints_ui',
      '#pre_render' => [
        [$class, 'preRenderMyElement'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderMyElement($element) {
    $breakpointInfo = \Drupal::service('breakpoints_ui')->getAllBreakpoints();
    $element['breakpoints_groups'] = $breakpointInfo;
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return [
      'preRenderMyElement',
    ];
  }

}
