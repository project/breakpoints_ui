<?php

namespace Drupal\breakpoints_ui;

use Drupal\Component\Serialization\Yaml;

/**
 * Class breakpointsUiService.
 *
 * @package Drupal\breakpoints_ui
 */
class BreakpointsUiService {

  /**
   * Get all breakpoints.
   *
   * @return array
   *   Returns the breakpoints.
   */
  public function getAllBreakpoints(): array {
    $breakpointsManager = \Drupal::service('breakpoint.manager');
    $getBreakpointsGroups = array_keys($breakpointsManager->getGroups());
    $breakpointInfo = [];
    foreach ($getBreakpointsGroups as $getBreakpointsGroup) {
      $typeExtension = implode(',', array_values($breakpointsManager->getGroupProviders($getBreakpointsGroup)));
      $projectPath = NULL;
      if ($typeExtension == 'theme') {
        $projectPath = \Drupal::service('extension.list.theme')->getPath($getBreakpointsGroup);
      }
      if ($typeExtension == 'module') {
        $projectPath = \Drupal::service('extension.list.module')->getPath($getBreakpointsGroup);
      }
      $breakPointYML = Yaml::decode(file_get_contents($projectPath . '/' . $getBreakpointsGroup . '.breakpoints.yml'));
      $breakpointInfo[$getBreakpointsGroup] = $breakPointYML;
    }
    return $breakpointInfo;
  }

}
