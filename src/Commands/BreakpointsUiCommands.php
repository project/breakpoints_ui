<?php

namespace Drupal\breakpoints_ui\Commands;

use Symfony\Component\Console\Helper\Table;
use Drupal\breakpoints_ui\BreakpointsUiService;
use Drush\Commands\DrushCommands;

/**
 * Breakpoints UI Drush Commands.
 */
class BreakpointsUiCommands extends DrushCommands {

  /**
   * The breakpoints UI service.
   *
   * @var \Drupal\breakpoints_ui\BreakpointsUiService
   */
  protected $breakpointsUiService;

  /**
   * Constructs BreakpointsUiCommands.
   *
   * @param \Drupal\breakpoints_ui\BreakpointsUiService $breakpoints_ui
   *   The breakpoints UI service.
   */
  public function __construct(BreakpointsUiService $breakpoints_ui) {
    parent::__construct();
    $this->breakpointsUiService = $breakpoints_ui;
  }

  /**
   * Breakpoints debug command.
   *
   * @command breakpoints:debug
   * @bootstrap full
   * @aliases bpd
   *
   * @usage breakpoints:debug
   */
  public function breakpointsDebug() {
    $breakpoints = $this->breakpointsUiService->getAllBreakpoints();

    $breakpoint_options = [
      'all' => 'All breakpoints',
    ];
    foreach ($breakpoints as $breakpoint_name => $breakpoint) {
      $breakpoint_options[$breakpoint_name] = ucwords($breakpoint_name);
    }
    $selection = $this->io()->choice('Select the breakpoint', $breakpoint_options, 0);
    foreach ($breakpoints as $breakpoint_name => $breakpoint) {
      if ($breakpoint_name == $selection || $selection == 'all') {
        $this->io()->section($breakpoint_name);
        $rows = [];
        foreach ($breakpoint as $child_breakpoint_name => $child_breakpoint) {
          $row = [];
          $row[] = $child_breakpoint_name;
          if (isset($child_breakpoint['label'])) {
            $row[] = $child_breakpoint['label'];
          }
          if (isset($child_breakpoint['mediaQuery'])) {
            $row[] = $child_breakpoint['mediaQuery'];
          }
          if (isset($child_breakpoint['multipliers'])) {
            $row[] = implode(',', $child_breakpoint['multipliers']);
          }
          $rows[] = $row;
        }
        $table = new Table($this->output);
        $table->setHeaders(['Machine Name', 'Label', 'Media Query', 'Multipliers'])->setRows($rows);
        $table->render();
        $this->io()->newLine();
      }
    }
  }
}
